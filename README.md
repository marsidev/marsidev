# 👋 Hello, I'm Luis Marsiglia.

- ⚡ Mobile Esports Data Analyst at [Tribe Gaming][tribegaming] 
- 👨‍💻 React and JavaScript beginner developer from Venezuela
- 🔭 Current projects I'm working on: Portfolio, [Climatic][climatic], [Wordly][wordly]
- 🌱 I'm interested on learn new and trending technologies
- ⭐ Preferred frontend techs: NextJS, Vite, ChakraUI
- ⭐ Preferred backend techs: NodeJS, Fastify, MongoDB
- 👨‍💻 Learning React since December 2021
- 💼 Open to work as Frontend Developer

---

### 🔗 Connect with me
<p>
<a href="mailto:marsiglia.business@gmail.com" title="marsiglia.business@gmail.com"><img align="center" src="https://img.shields.io/badge/Email-c14438?style=flat-square&logo=gmail&logoColor=white&link=mailto:marsiglia.business@gmail.com" alt="gmail badge" /></a>
<a href="https://twitter.com/marsigliacr" title="@marsigliacr on Twitter"><img align="center" src="https://img.shields.io/badge/@marsigliacr-1DA1F2?style=flat-square&logo=twitter&logoColor=white&link=mailto:marsiglia.business@gmail.com" alt="twitter badge"/></a>
<a href="https://www.linkedin.com/in/marsidev" title="@marsidev on Linkedin"><img align="center" src="https://img.shields.io/badge/@marsidev-0A66C2?style=flat-square&logo=linkedin&logoColor=white&link=mailto:marsiglia.business@gmail.com" alt="linkedin badge"/></a>
</p>

---

### 🛠️ Tech 
<p align="left"> 
  <img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=flat-square&logo=javascript&logoColor=black" alt="javascript badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/TypeScript-3178C6?style=flat-square&logo=typescript&logoColor=white" alt="typescript badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/Node.js-43853D?style=flat-square&logo=node.js&logoColor=white" alt="nodejs badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/React-007096?style=flat-square&logo=react&logoColor=white" alt="react badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/Next.js-000000?style=flat-square&logo=next.js&logoColor=white" alt="nextjs badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/Vite-646CFF?style=flat-square&logo=vite&logoColor=white" alt="vite badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/Express.js-000?style=flat-square&logo=express" alt="expressjs" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/Fastify-000?style=flat-square&logo=fastify" alt="fastify badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/HTML5-E34F26?style=flat-square&logo=css3&logoColor=white" alt="html5 badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/CSS3-1572B6?style=flat-square&logo=css3&logoColor=white" alt="css3 badge" style="vertical-align:top; margin:4px">
  <img src="https://img.shields.io/badge/MongoDB-47A248?style=flat-square&logo=mongodb&logoColor=white" alt="mongodb" style="vertical-align:top; margin:4px">
</p>

---

<!-- ### ✨ Projects
<div style="display:flex; justify-content:center; align-items:center; flex-direction:row; max-width:100%; gap:1em; flex-wrap:wrap;">
  <a href="https://github.com/marsidev/climatic" style="margin:4px">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=marsidev&repo=climatic&hide_border=false&border_radius=16&&disable_animations=tru&theme=buefy" alt="" style=""  />
  </a>
  <a href="https://github.com/marsidev/wordly" style="margin:4px">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=marsidev&repo=wordly&hide_border=false&border_radius=16&&disable_animations=true&theme=buefy" alt="" style=""  />
  </a>
  <a href="https://github.com/marsidev/overnote" style="margin:4px">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=marsidev&repo=overnote&hide_border=false&border_radius=16&&disable_animations=true&theme=buefy" alt="" style=""  />
  </a>
  <a href="https://github.com/marsidev/AxieHub" style="margin:4px">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=marsidev&repo=AxieHub&hide_border=false&border_radius=16&disable_animations=true&theme=buefy" alt="" style=""  />
  </a>
  <a href="https://github.com/marsidev/get-sc-key" style="margin:4px">
    <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=marsidev&repo=get-sc-key&hide_border=false&border_radius=16&disable_animations=true&theme=buefy" alt="" style=""  />
  </a>
</div>

--- -->

### 📈 Stats
![Commits Streak](https://github-readme-streak-stats.herokuapp.com/?user=marsidev&theme=buefy)
![Github Stats](https://github-readme-stats.vercel.app/api?username=marsidev&count_private=true&show_icons=true&count_private=true&border_radius=16&locale=en&include_all_commits=true&count_private=true&custom_title=GitHub%20Stats&disable_animations=false&theme=buefy)
![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=marsidev&hide=TeX,Procfile&layout=compact&border_radius=16&locale=en&disable_animations=false&theme=buefy)
![Wakatime Stats](https://github-readme-stats.vercel.app/api/wakatime?username=marsidev&border_radius=16&layout=default&theme=buefy&langs_count=10&hide=json,bash,yaml,reStructuredText,Git%20Config)

<!-- ![Visitor Badge](https://visitor-badge.laobi.icu/badge?page_id=marsidev) -->
![Visitor Badge](https://komarev.com/ghpvc/?username=marsidev&label=Profile%20views&color=0e75b6&style=flat-square)
![Wakatime Stats](https://wakatime.com/badge/user/7fee11fb-f30c-4ec4-9052-d9f582b1ebc4.svg?style=flat-square)

*Note: Wakatime related data begins at Jun 08, 2022 (Date when I installed it).*

<!--START_SECTION:waka-->
**🐱 My GitHub Data** 

> 🏆 1,622 Contributions in the Year 2022
 > 
> 📦 161.1 kB Used in GitHub's Storage 
 > 
> 🚫 Not Opted to Hire
 > 
> 📜 20 Public Repositories 
 > 
> 🔑 4 Private Repositories  
 > 
**I'm a Night 🦉** 

```text
🌞 Morning    22 commits     ░░░░░░░░░░░░░░░░░░░░░░░░░   2.37% 
🌆 Daytime    317 commits    ████████░░░░░░░░░░░░░░░░░   34.16% 
🌃 Evening    435 commits    ███████████░░░░░░░░░░░░░░   46.88% 
🌙 Night      154 commits    ████░░░░░░░░░░░░░░░░░░░░░   16.59%

```
📅 **I'm Most Productive on Saturday** 

```text
Monday       101 commits    ██░░░░░░░░░░░░░░░░░░░░░░░   10.88% 
Tuesday      81 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   8.73% 
Wednesday    106 commits    ██░░░░░░░░░░░░░░░░░░░░░░░   11.42% 
Thursday     137 commits    ███░░░░░░░░░░░░░░░░░░░░░░   14.76% 
Friday       166 commits    ████░░░░░░░░░░░░░░░░░░░░░   17.89% 
Saturday     235 commits    ██████░░░░░░░░░░░░░░░░░░░   25.32% 
Sunday       102 commits    ██░░░░░░░░░░░░░░░░░░░░░░░   10.99%

```


📊 **This Week I Spent My Time On** 

```text
⌚︎ Time Zone: America/Caracas

💬 Programming Languages: 
TypeScript               43 hrs 40 mins      ███████████████████░░░░░░   75.84% 
JSON                     9 hrs 56 mins       ████░░░░░░░░░░░░░░░░░░░░░   17.25% 
Markdown                 1 hr 37 mins        ░░░░░░░░░░░░░░░░░░░░░░░░░   2.83% 
reStructuredText         1 hr 1 min          ░░░░░░░░░░░░░░░░░░░░░░░░░   1.77% 
CSS                      19 mins             ░░░░░░░░░░░░░░░░░░░░░░░░░   0.58%

🔥 Editors: 
VS Code                  57 hrs 35 mins      █████████████████████████   100.0%

🐱‍💻 Projects: 
climatic                 57 hrs 35 mins      █████████████████████████   100.0%

💻 Operating System: 
Windows                  57 hrs 35 mins      █████████████████████████   100.0%

```

**I Mostly Code in JavaScript** 

```text
JavaScript               12 repos            ████████████████░░░░░░░░░   66.67% 
TypeScript               4 repos             █████░░░░░░░░░░░░░░░░░░░░   22.22% 
Zig                      1 repo              █░░░░░░░░░░░░░░░░░░░░░░░░   5.56% 
CSS                      1 repo              █░░░░░░░░░░░░░░░░░░░░░░░░   5.56%

```



 Last Updated on 23/06/2022 00:31:56 UTC
<!--END_SECTION:waka-->

Stats provided by [github-readme-stats](https://github.com/anuraghazra/github-readme-stats), [waka-readme-stats](https://github.com/anmol098/waka-readme-stats), and [github-readme-streak-stats](https://github.com/DenverCoder1/github-readme-streak-stats).

---

### Support me
<p>
  <a href="https://www.buymeacoffee.com/marsi" title="https://www.buymeacoffee.com/marsi">
    <img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="auto" alt="Buy me a cofee in buymeacoffee.com"  />
  </a>
  <a href="https://ko-fi.com/marsidev" title="https://ko-fi.com/marsidev">
    <img src="https://cdn.ko-fi.com/cdn/kofi3.png?v=3" height="50" width="auto" alt="Buy me a cofee in ko-fi.com"  />
  </a>
  <a href="https://cafecito.app/marsi" title="https://cafecito.app/marsi">
    <img src="https://cdn.cafecito.app/imgs/buttons/button_6.svg" height="50" width="auto" alt="Invitame un café en cafecito.app" />
  </a>
</p>

[twitter]: https://twitter.com/marsigliacr
[tribegaming]: https://twitter.com/tribegaming
[climatic]: https://github.com/marsidev/climatic
[wordly]: https://github.com/marsidev/wordly

<!-- widgets and icons reference -->
<!-- https://github.com/anuraghazra/github-readme-stats -->
<!-- https://git.io/streak-stats -->
<!-- https://rahuldkjain.github.io -->
<!-- https://simpleicons.org -->
<!-- https://img.shields.io/ -->
